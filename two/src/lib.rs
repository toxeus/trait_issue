pub trait Foo {
    fn foo() -> String;
}

#[cfg(test)]
mod test {
    use one::Mock;
    pub struct Baz<T: super::Foo> {
        pub foo: T,
    }

    #[test]
    fn foo_test() {
        let a = Baz { foo: Mock {} };
        assert_eq!(a.foo.foo(), "asdf".into());
    }
}
